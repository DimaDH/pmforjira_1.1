<?php


namespace App\Helpers;

use App\Developer;
use App\Helpers\Contracts\DeveloperInterface;
use App\Project;
use Carbon\Carbon;


class DeveloperDb implements DeveloperInterface  {


    /**
     * When you change the date of the project for which it has developers.
     * The method is called "checkBusy" checking the busy programmer in this interval of time.
     *
     * @param string $start
     * @param string $end
     * @param integer $id
     * @return boolean
     *
     * Return boolean value
     */
    public static function checkBusy($start, $end, $id)
    {
            $this_obj = new self();
            $start =  $this_obj->CreateDateFormat($start);
            $end =  $this_obj->CreateDateFormat($end);
            $arr = [];



            $arrDev = Project::find($id)->developers;

            foreach ($arrDev as $dev){
                array_push($arr, Developer::find($dev->id)->projects->where('start','>=',$start)
                                                                    ->where('start','<=',$end)
                                                                    ->where('id', '!=', $id));

                array_push($arr, Developer::find($dev->id)->projects->where('end','>=',$start)
                                                                    ->where('end','<=',$end)
                                                                    ->where('id', '!=', $id));
            }

            foreach ($arr as $item){
                if(count($item) !== 0){
                    return false;
                }
            }

            return true;
    }


    /**
     * This method makes the date format Y-m-d H:i:s
     *
     * @param string $dateCreate
     * @return carbon
     *
     * Return carbon data
     */

    public static function CreateDateFormat( $dateCreate ) {
        $date = explode( " ", $dateCreate );
        $start_time = $date[1];

        $arr_time = explode( ":", $start_time );
        $start = "0000-00-00 00:00:00.000000";

        if( count( $arr_time ) == 0 )
            $start = Carbon::createFromFormat( 'Y-m-d', $dateCreate );

        if( count( $arr_time ) == 1 )
            $start = Carbon::createFromFormat( 'Y-m-d H', $dateCreate );

        if( count( $arr_time ) == 2 )
            $start = Carbon::createFromFormat( 'Y-m-d H:i', $dateCreate );

        if( count( $arr_time ) == 3 )
            $start = Carbon::createFromFormat( 'Y-m-d H:i:s', $dateCreate );

        return $start;
    }

    /**
     * When the developer is assigned to the project,
     * the method "checkBusyDevUnderDistribution"
     * is used to check the free programmer at this time.
     *
     * @param array $arrDeveloper
     * @param integer $id
     * @return array
     *
     * Returns an array with busy developers
     */
    public static function checkBusyDevUnderDistribution($arrDeveloper,$id)
    {
            $this_obj = new self();
            $arr = [];
            if(isset($arrDeveloper)) {
                $start = $this_obj->CreateDateFormat(Project::find($id)->start);
                $end = $this_obj->CreateDateFormat(Project::find($id)->end);

                foreach ($arrDeveloper as $dev) {
                    if (count(Developer::find($dev)->projects->where('start', '>=', $start)
                                                             ->where('start', '<=', $end)
                                                             ->where('id', '!=', $id)) > 0
                    )
                        array_push($arr, Developer::find($dev)->title);

                    if (count(Developer::find($dev)->projects->where('end', '>=', $start)
                            ->where('start', '<=', $end)
                            ->where('id', '!=', $id)) > 0
                    )
                        array_push($arr, Developer::find($dev)->title);
                }
                $arr = array_unique($arr);
                foreach ($arr as $item) {
                    if (count($item) !== 0) {
                        return $arr;
                    }
                }
            }

            return $arr;

    }


    /**
     * This method makes the
     * assignment of developers to the project.
     *
     * @param array $arrDeveloper
     * @param integer $id
     * @return array
     *
     * Returns an array with busy developers
     */
    public static function appointmentOfDevelopers($arrDeveloper, $id)
    {
            $project = Project::find($id);
            $project->developers()->detach();                //  need remake
            $project->developers()->attach($arrDeveloper);
            return $project->developers;
    }


}