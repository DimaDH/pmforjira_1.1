<?php

namespace App\Helpers\Contracts;

use App\Project;
use Illuminate\Http\Request;

Interface ProjectInterface{
    public static function getAllProjectAndDeveloper();
    public static function getAllProjectWithOutDeveloper();
    public static function getDeveloper();
    public static function create($project, $name, $key, $projectTypeKey, $lead, $start, $end, $distribution);
}
