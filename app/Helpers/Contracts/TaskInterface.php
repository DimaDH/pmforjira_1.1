<?php

namespace App\Helpers\Contracts;

use Illuminate\Http\Request;

Interface TaskInterface{

    public static function appointmentOfTasks( $arrTask, $id );

    public static function createOrUpdate( $id , $summary, $key, $resolution, $status, $color, $priority_id, $project_id, $descriptions, $labels );
}
