<?php

namespace App\Helpers\Contracts;

use Illuminate\Http\Request;

Interface DeveloperInterface{
    public static function checkBusy($start, $end, $id);
    public static function CreateDateFormat($dateCreate);
    public static function checkBusyDevUnderDistribution($arrDeveloper,$id);
    public static function appointmentOfDevelopers($arrDeveloper, $id);

}
