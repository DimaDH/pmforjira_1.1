<?php


namespace App\Helpers;

use App\DescriptionTask;
use App\Helpers\Contracts\TaskInterface;
use App\Project;
use App\Specialie;
use App\Task;
use Illuminate\Http\Request;


class TaskDb implements TaskInterface {


    /**
     * This method assigns tasks to the project.
     *
     * @param array $arrTask
     * @param integer $id
     * @return array
     *
     * Returns an array of tasks assigned to the project
     */
    public static function appointmentOfTasks( $arrTask, $id )
    {
            $project= Project::find( $id );
            $arrId = [];
            foreach ($project->tasks as $task){
                array_push($arrId , $task->id);
            }

            //reset
            $tasks = Task::whereIn('id',$arrId);
            $tasks->update(['project_id' => 1]);

            //set
            $tasks = Task::whereIn('id',$arrTask);
            $tasks->update(['project_id' => $id]);

            return $project->tasks;
    }


    /**
     * This method creates or updates a task.
     *
     * @param integer $id
     * @param string $summary
     * @param string $key
     * @param string $resolution
     * @param string $status
     * @param string $color
     * @param integer $priority_id
     * @param integer $project_id
     * @param string $descriptions
     * @param string $labels
     *
     * Nothing returns
     */
    public static function createOrUpdate( $id , $summary, $key, $resolution, $status, $color, $priority_id, $descriptions, $labels, $project_id = 1)
    {
        if ( $id != null ) {
            $newTask = Task::find( $id );
        }else{
            $newTask = new Task();
        }
        $newTask->summary = $summary;
        $newTask->key = $key;
        $newTask->resolution = $resolution;
        $newTask->status = $status;
        $newTask->color = $color;
        $newTask->priority_id = $priority_id;
        $newTask->project_id = $project_id;
        $newTask->save();

        if( $descriptions !== null ) {
            $desTask = new DescriptionTask();
            $desTask->name = $descriptions;
            $desTask->task_id = $newTask->id;
            $desTask->save();
        }


        $arrIdSpeciality = [];
        if ( ( isset( $labels ) ) ) {
            for( $i = 0 ;$i < ( count( $labels ) ); $i++ ) {
                $idSpeciality = Specialie::where( 'name', $labels[ $i ] )->first();
                if ( $idSpeciality === null ) {
                    $specTask = new Specialie();
                    $specTask->name = $labels[ $i ];
                    $specTask->save();
                    array_push( $arrIdSpeciality, $specTask->id );
                } else {
                    array_push( $arrIdSpeciality, $idSpeciality->id );
                }
            }
        }

        $taskSpeciality = Task::find( $newTask->id );
        $taskSpeciality->specialities()->detach();
        $taskSpeciality->specialities()->attach( $arrIdSpeciality );
    }


}