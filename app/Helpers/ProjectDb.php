<?php


namespace App\Helpers;

use App\DescriptionProject;
use App\Developer;
use App\Helpers\Contracts\ProjectInterface;
use App\Project;


class ProjectDb implements ProjectInterface  {

    /**
     * This method returns all projects
     * that are assigned to the developers.
     *
     * @return array
     *
     * Returns an array projects
     */
    public static function getAllProjectAndDeveloper(){
        $arr =[];
        $data =  Developer::get( ['id','email'] );
        foreach ( $data as $dev ) {
            if( isset( $dev->projects ) ) {
                foreach ( $dev->projects as $it ) {
                    $obj = new \stdClass();
                    $obj->id = $it->id;
                    $obj->title = $it->name;
                    $obj->start = $it->start;
                    $obj->end = $it->end;
                    $obj->resourceId = $dev->email;
                    array_push( $arr, $obj );
                }
            }
        }

        $projects =  Project::get( ['id','name','start','end'] );
        foreach ( $projects as $it ) {
            if( !isset( $it->developers[0] ) ) {
                $obj = new \stdClass();
                $obj->id = $it->id;
                $obj->title = $it->name;
                $obj->start = $it->start;
                $obj->end = $it->end;
                $arrDateStart = explode( " ", $it->start );
                $arrDateEnd = explode( " ", $it->end );
                if( isset( $arrDateStart[1] ) )
                    if ( $arrDateStart[1] == "00:00:00" ) {
                        $obj->start = isset( $arrDateStart[0] ) ? $arrDateStart[0] : "";
                    }
                if( isset( $arrDateEnd[1] ) )
                    if ( $arrDateEnd[1] == "00:00:00" ) {
                        $obj->end = isset( $arrDateEnd[0] ) ? $arrDateEnd[0] : "";
                    }
                array_push( $arr, $obj );
            }
        }

        return $arr;
    }

    /**
     * This method returns all projects
     * that are not assigned to developers.
     *
     * @return array
     *
     * Returns an array projects
     */
    public static function getAllProjectWithOutDeveloper()
    {
        $arr = [];
        $projects  =  Project::get( ['id','name','start','end'] );
        foreach ( $projects as $it ) {
            $obj = new \stdClass();
            $obj->id = $it->id;
            $obj->title = $it->name;
            $obj->start = $it->start;
            $obj->end = $it->end;
            $arrDateStart = explode( " ", $it->start );
            $arrDateEnd = explode( " ", $it->end );
            if( isset( $arrDateStart[1] ) )
                if ( $arrDateStart[1] == "00:00:00" ) {
                    $obj->start = isset($arrDateStart[0]) ? $arrDateStart[0] : "";
                }
            if( isset( $arrDateEnd[1] ) )
                if ( $arrDateEnd[1] == "00:00:00" ) {
                    $obj->end = isset($arrDateEnd[0]) ? $arrDateEnd[0] : "";
                }
            array_push( $arr, $obj);
        }


        return $arr;
    }

    /**
     * This method returns all developers
     *
     * @return array
     *
     * Returns an array developers
     */
    public static function getDeveloper()
    {
        $arr = [];
        $developers = Developer::get( ['id', 'email','title'] );
        foreach ( $developers as $developer){
            $obj = new \stdClass();
            $obj->id = $developer->email;
            $obj->title = $developer->title;
            array_push( $arr, $obj);
        }
        return  $arr;
    }

    /**
     * This method create project
     *
     * @param Project $project
     * @param string $name
     * @param string $key
     * @param string $projectTypeKey
     * @param string $lead
     * @param string $start
     * @param string $end
     * @param null $descriptions
     *
     * Nothing returns
     */
    public static function create($project, $name, $key, $projectTypeKey, $lead, $start = '1970-01-01 00:00:00', $end = '1970-01-01 00:00:00', $descriptions = null ){

        $name != null ? $project->name = $name : false;
        $lead != null ? $project->lead = $lead : false;
        $key != null ? $project->key = $key : false;
        $projectTypeKey !== null ? $project->projectTypeKey = $projectTypeKey : false;
        $project->start = $start;
        $project->end = $end;
        $project->save();


        if ( $descriptions != null ) {
            DescriptionProject::where( 'project_id', $project->id )->delete();
            $descriptionNew = new DescriptionProject();
            $descriptionNew->name = $descriptions;
            $descriptionNew->project_id = $project->id;
            $descriptionNew->save();
        }
    }

    /**
     * This method update project
     *
     * @param $id
     * @param string $start
     * @param string $end
     *
     * Nothing returns
     */
    public static function updateDateProject($id,  $start, $end){
        $msg = Project::find( $id );
        $msg->start = $start;
        $msg->end = $end;
        $msg->save();
    }

}
