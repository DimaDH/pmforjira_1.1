<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriorityTask extends Model
{
    protected $table = 'priority_task';

    public function task(){
        return $this->belongsTo('App\Task');
    }
}
