<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.07.17
 * Time: 0:53
 */

namespace App\Providers;

use App\JiraRequestModule\UploadJira;
use Illuminate\Support\ServiceProvider;


class JiraServiceProvider extends ServiceProvider

{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes( [
            __DIR__ . '/config/jira.php' => config_path( 'jira.php' ),
        ] );
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom( __DIR__ . '/config/jira.php', 'jira' );
        $this->app['jira'] = $this->app->share( function ( $app )
        {
            return new UploadJira;
        } );
    }
}