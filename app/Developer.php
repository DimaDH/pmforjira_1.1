<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Developer extends Model
{
    //
    protected $fillable = ['id','title', 'email','availablePerWeek','level_id','active'];

    public function level(){
        return $this->hasOne('App\Level','id', 'level_id');
    }

    public function projects(){
        return $this->belongsToMany('App\Project','project_developer', 'developer_id', 'project_id');
    }

    public function specialities(){
        return $this->belongsToMany('App\Developer','developer_specialie', 'developer_id', 'speciality_id');
    }

}
