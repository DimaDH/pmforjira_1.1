<?php

namespace App\Http\Controllers;

use App\DescriptionTask;
use App\Facades\TaskFacade;
use App\PriorityTask;
use App\Project;
use App\Specialie;
use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{


    /**
     * This method returns task view.
     *
     * Return view.
     */
    public function index()
    {
        return view( "task.task" );
    }


    /**
     * This method creates new task.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     *
     * Return make redirect on step before.
     */
    public function create( Request $request )
    {
        $task = $request->all();
            TaskFacade::createOrUpdate(
                isset($task['id_Task']) ? $task['id_Task'] : null,
                isset($task['summary']) ? $task['summary'] : null,
                isset($task['key']) ? $task['key'] : null,
                isset($task['resolution']) ? $task['resolution'] : null,
                isset($task['status']) ? $task['status'] : null,
                '#fff',
                isset($task['priority']) ? $task['priority'] : null,
                isset($task['descriptions']) ? $task['descriptions'] : null,
                isset($task['labels']) ? $task['labels'] : null,
                isset($task['project_id']) ? $task['project_id'] : null
            );


        $url = '/project/add/task/'.$task[ 'project_id' ];
        return redirect( $url );

    }


    /**
     * This method assigns tasks to the project
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     *
     * return json
     */
    public function edit( Request $request, $id )
    {
        if ( $request->isMethod( 'GET' ) ) {
            return response()->json( [ 'response' => TaskFacade::appointmentOfTasks( $request[ 'tasks' ], $id ) ] );
        }
    }


    /**
     * This method makes destroy Task.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     *
     * return json
     */
    public static function destroy( $id )
    {
        $task = Task::find( $id );

        $task->specialities()->detach();
        $task->delete();

        DescriptionTask::where( 'task_id', $id )->delete();

        return response()->json( [ 'response' => true ] );
    }

    /**
     * This method makes render view.
     *
     * @return \Illuminate\Http\Response
     *
     * return view
     */
    public function reloadTaskFormDb()
    {
        $project = Project::whereDate( 'start', '=', '1970-01-01 00:00:00' )->get( [ 'id', 'name', 'start', 'end' ] );

        return response()->json( [ 'response' => view( 'task.createTask' , [ 'project' => $project ] )->render() ] );
    }
}
