<?php

namespace App\Http\Controllers;

use App\Developer;
use App\Facades\DeveloperFacade;
use App\Project;
use Illuminate\Http\Request;

class DeveloperController extends Controller
{


    /**
     * The method checks the busy programmer in this interval of time.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     *
     * Return json
     */
    public function checkBusy( Request $request, $id )
    {
        if ($request->isMethod('post')) {
            return response()->json( [ 'response' => DeveloperFacade::checkBusy( $request->start, $request->end, $id ) ] );
        }

    }

    /**
     * The method uses to check the free programmer at this time.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     *
     * Return json
     */
    public function checkBusyDevUnderDistribution( Request $request, $id )
    {
        if ($request->isMethod('get')) {
            return response()->json( [ 'response' => DeveloperFacade::checkBusyDevUnderDistribution( $request->developers, $id ) ] );
        }

    }

    /**
     * This method makes the
     * assignment of developers to the project.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     *
     * Return json
     */
    public function appointmentOfDevelopers( Request $request, $id )
    {
        if ( $request->isMethod( 'get' ) ) {
            return response()->json( [ 'response' => DeveloperFacade::appointmentOfDevelopers( $request['developers'], $id ) ] );
        }
    }

}
