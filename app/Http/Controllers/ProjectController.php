<?php

namespace App\Http\Controllers;

use App\DescriptionTask;
use App\Developer;
use App\PriorityTask;
use App\Project;
use App\Specialie;
use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\JiraRequestModel\UploadJira;
use Illuminate\Support\Facades\Auth;
use App\DescriptionProject;
use App\Facades\DeveloperFacade;
use App\Facades\TaskFacade;
use App\Facades\ProjectFacade;

class ProjectController extends Controller
{

    /**
     * This method returns all projects.
     * that are assigned to the developers.
     *
     * Return json.
     */
    public function index()
    {
        return Response()->json( ProjectFacade::getAllProjectAndDeveloper() );
    }


    /**
     * This method returns all projects.
     * that are not assigned to developers.
     *
     * Return json.
     */
    public function withOutDeveloper()
    {
        return Response()->json( ProjectFacade::getAllProjectWithOutDeveloper() );
    }


    /**
     * This method returns all developers
     *
     * Return json.
     */
    public function developer()
    {
        return Response()->json( ProjectFacade::getDeveloper() );
    }

    /**
     * This method create or update project.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * return redirect to rout home.
     */
    public function store( Request $request )
    {

        $project = isset( $request['checkbox'] ) ? Project::find( $request['project'] ) : new Project();

        ProjectFacade::create(  $project,
            isset( $request[ 'title' ] ) ? $request[ 'title' ] : null,
            isset( $request[ 'key' ] ) ? $request[ 'key' ] : null,
            isset( $request[ 'projectTypeKey' ] ) ? $request[ 'projectTypeKey' ] : null,
            isset( Auth::user()->name ) ? Auth::user()->name : null,
            $request['start-date'] . " " . $request['start-time'],
            $request['end-date'] . " " . $request['end-time'],
            isset( $request[ 'distribution' ] ) ? $request[ 'distribution' ] : null
            );




        return redirect()->route( 'home' );
    }

    /**
     * This method show project by id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
     * return json
     */
    public function show( $id )
    {
        $project = Project::find( $id );

        return response()->json( [ 'response' => view( 'project.infoProject', ['project' => $project] )->render() ] );
    }


    /**
     * This method takes two method( Get, Post ).
     *
     * Post returns json includes view which make update or create task.
     *
     * Get returns view main page.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     *
     * Return view.
     */
    public function addTask( Request $request, $id )
    {

        if ( $request->isMethod( 'post' ) ) {
            if ( $id != 0 ) {
                $task= Task::find( $id );
                return response()->json( [ 'response' => view( 'task.task', [ 'task' => $task, 'project_id' => $request[ 'project_id' ], 'button' => 'Update' ] )->render() ] );
            }else{
                return response()->json( [ 'response' => view( 'task.task', [ 'task' => new Task(), 'project_id' => $request[ 'project_id' ], 'button' => 'Add' ] )->render() ] );
            }
        }


        if ( $request->isMethod( 'get' ) ) {
            $data = [
                'tasks' => Task::get( ['id', 'summary','project_id'] ),
                'project' => Project::find( $id ),
            ];
            return view( "task.addTask",$data);
        }
    }

    /**
     * This method takes two method( Get, Post ).
     *
     * Post returns json includes view.
     * Get returns view main page.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function addDev( Request $request, $id)
    {

        if ( $request->isMethod( 'post' ) ) {
            $developer = Developer::find( $id );
            return response()->json( ['response' => view( 'developer.developer', ['developer' => $developer] )->render() ] );
        }


        if ( $request->isMethod( 'get') ) {
            $data = [
                'developers' => Developer::get( ['id', 'title']),
                'project' => Project::find( $id),
            ];
            return view( "developer.addDeveloper", $data );
        }

    }


    /**
     * This method update project.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
     * return json.
     */
    public function update( Request $request, $id )
    {
        ProjectFacade::updateDateProject($id,  $request->start, $request->end);
        return response()->json( ['response' => true ] );

    }

    /**
     * This method update project when date and time was separated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * return json.
     */
    public function updateWithSeparateDate( Request $request)
    {
            $project = Project::find( $request['id'] );
            ProjectFacade::create(  $project,
                                    isset( $request[ 'title' ] ) ? $request[ 'title' ] : null,
                                    Auth::user()->name,
                                    isset( $request[ 'key' ] ) ? $request[ 'key' ] : null,
                                    isset( $request[ 'projectTypeKey' ] ) ? $request[ 'projectTypeKey' ] : null,
                                    $request['start-date'] . " " . $request['start-time'],
                                    $request['end-date'] . " " . $request['end-time'],
                                    isset( $request[ 'distribution' ] ) ? $request[ 'distribution' ] : null );

            return redirect()->route( 'home' );
    }

    /**
     * This method update date-end project when the project size changes in the calendar.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * return json.
     */
    public function updateResize( Request $request, $id )
    {
            $msg = Project::find( $id );
            $end = $msg->end;
            $resEnd = Carbon::createFromFormat( 'Y-m-d H:i:s', $end );
            $msg->end = $resEnd->addSeconds( $request->dayDelta );
            $msg->save();
            return response()->json( ['response' => true] );

    }
    /**
     * This method destroy Project by id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        $project = Project::find( $id );

        $project->developers()->detach();
        $project->delete();

        DescriptionProject::where( 'project_id', $id )->delete();

        return response()->json( ['response' => true] );
    }

    /**
     * This method make download from Jira and add to Db
     *
     * @return \Illuminate\Http\Response
     */
    public function download()
    {
        //Project
        $projects = UploadJira::searchForUploadProject();
        if ( isset( $projects ) ) {
            foreach ( $projects as $project ) {
                $foundProject = Project::where( 'name', '=', $project->name )->first();
                if ( $foundProject === null ) {
                    ProjectFacade::create(  new Project(),
                                            $project->name,
                                            $project->key,
                                            $project->projectTypeKey,
                                            $project->lead->displayName);
                }
            }
        }


        //Developer
        $responseProgrammer = array( );
        $uploadProgrammer = UploadJira::searchUserForUpload( );
        $d = null;
        foreach ( $uploadProgrammer as $key => $val ) {
            if ( $key % 2 == 0) {
                $d = new \stdClass();
                $d->displayname = $val;
            } else {
                $d->email = $val;
                array_push( $responseProgrammer, $d );
            }
        }
        $developers = $responseProgrammer;
        foreach ( $developers as $it ) {
            $findProject = Developer::where( [
                ['title', '=', $it->displayname],
                ['email', $it->email],
            ] )->first();
            if ( $findProject === null ) {
                Developer::create( [
                    'title' => $it->displayname,
                    'email' => $it->email
                ]);
            }
        }


        //Task
        $tasks = UploadJira::searchTaskForUpload( );
        if ( isset( $tasks ) ) {
            foreach ( $tasks as $task) {
                $findProject = Task::where( 'summary', '=', $task->fields->summary)->first();
                if ( $findProject === null ) {
                    TaskFacade::createOrUpdate( null,
                        $task->fields->summary,
                        $task->key,
                        $task->fields->resolution,
                        $task->fields->status->name,
                        '#fff',
                        PriorityTask::where( 'name', $task->fields->priority->name )->first()->id,
                        $task->fields->description,
                        $task->fields->labels
                    );
                }
            }
        }
        return response()->json( ['response' => true] );

    }

    /**
     * This method return view
     *
     * @return \Illuminate\Http\Response
     *
     * return main of project view
     */
    public function reloadProjectFormDb()
    {
        $date = [
            'project' => Project::whereDate( 'start', '=', '1970-01-01 00:00:00' )->get( [ 'id', 'name' ] )
        ];
        return view( 'project.distribution', $date );
    }

    /**
     * This method sends data on view for rendering.
     *
     * @param $id
     * @return \Illuminate\Http\Response return main of project view
     *
     * return json with rendered view.
     */
    public function updateProject( $id )
    {

        $projectById = Project::find( $id );

        $project = new \stdClass();
        $project->id = isset($projectById->id) ?  $projectById->id :  "";
        $project->name = isset($projectById->name) ? $projectById->name :  "";

        $date = explode(" ", $projectById->start );
        $project->startD = isset($date[0]) ? $date[0] :  "";
        $project->startT = isset($date[1]) ? $date[1] :  "";

        $date = explode(" ", $projectById->end );
        $project->endD = isset( $date[0] ) ? $date[0] : "";
        $project->endT = isset( $date[1] ) ? $date[1] : "";

        $project->descriptions = isset( $projectById->descriptions[0]->name ) ? $projectById->descriptions[0]->name : "";

        return response()->json( [ 'response' => view( 'project.updateProject', [ 'project' => $project ] )->render() ] );
    }
}
