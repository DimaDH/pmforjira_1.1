<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialie extends Model
{
    public function tasks(){
        return $this->belongsToMany('App\Task','task_specialie', 'speciality_id', 'task_id');
    }

    public function developers(){
        return $this->belongsToMany('App\Developer','developer_specialie', 'speciality_id', 'developer_id');
    }
}
