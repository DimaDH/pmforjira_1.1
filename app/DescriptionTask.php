<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DescriptionTask extends Model
{
    protected $table = 'description_task';

    public function task(){
        return $this->belongsTo('App\Task');
    }
}
