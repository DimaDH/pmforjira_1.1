<?php

namespace App\JiraRequestModel;

use Illuminate\Database\Eloquent\Model;

class UploadJira extends Model
{
    /**
     * Search function to search project
     *
     * @return mixed
     */
    public static function searchForUploadProject()
    {
        $projetc = array();
        $result = json_decode(self::request('project'));
        foreach ($result as $project_result) {
            $upload_result = json_decode(self::request('project/' . $project_result->key));
            array_push($projetc, $upload_result);
        }
        return $projetc;
    }

    /**
     * Search function to search Task
     *
     * @return mixed
     */
    public static function searchTaskForUpload()
    {
        $tasks = array();
        $result = json_decode(self::request( 'search' ));
        foreach ($result->issues as $task_result)
            array_push($tasks , $task_result);
        return $tasks;
    }

    /**
     * Search function to search Users via alphabet
     *
     * @return mixed
     */
    public static function searchUserForUpload()
    {
        $user = array();
        $items = array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o",
            "p","q","r","s","t","u","v","w","x","y","z");
        foreach ($items as $item) {
            $result = json_decode(self::request('user/search?username=' . $item));
            foreach ($result as $add_result) {
                array_push($user, $add_result->displayName);
                array_push($user, $add_result->emailAddress);
            }
        }
        $concut_user = array_unique($user);
        return $concut_user;
    }

    /**
     * CURL request to the JIRA REST api
     *
     * @param $request
     * @param int $is_post
     * @param int $is_put
     * @return mixed
     */
    private static function request( $request, $is_post = 0, $is_put = 0 )
    {
        $ch = curl_init();
        curl_setopt_array( $ch, array(
            CURLOPT_URL            => config( 'jira.url' ) . '/rest/api/2/' .$request,
            CURLOPT_USERPWD        => config( 'jira.username' ) . ':' . config( 'jira.password' ),
            CURLOPT_HTTPHEADER     => array( 'Content-type: application/json' ),
            CURLOPT_RETURNTRANSFER => 1,
        ) );
        if( $is_post )
        {
            curl_setopt( $ch, CURLOPT_POST, 1 );
        }
        if( $is_put )
        {
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'PUT' );
        }
        $response = curl_exec( $ch );
        curl_close( $ch );
        return $response;
    }
}
