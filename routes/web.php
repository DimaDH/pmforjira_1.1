<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Auth::routes();


Route::group(['middleware' => ['auth', 'web']], function () {
    Route::get('/', 'ProjectController@reloadProjectFormDb')->name('home');
    Route::get('/developerlist', 'ProjectController@developer');
    Route::post('/updateWithSeparateDate', 'ProjectController@updateWithSeparateDate');
    Route::get('/projectlist', 'ProjectController@withOutDeveloper');
    Route::get('/reloadTaskFormDb', 'TaskController@reloadTaskFormDb');
    Route::post('/updateProject/{id}', 'ProjectController@updateProject');
    Route::post('/developercheck/{id}', 'DeveloperController@checkBusy')->name('checkBusy');
    Route::get('/developercheck/{id}', 'DeveloperController@checkBusyDevUnderDistribution')->name('checkBusyDevUnderDistribution');
    Route::get('developer/{developer}/edit', 'DeveloperController@appointmentOfDevelopers')->name('appointmentOfDevelopers');
    Route::post('/download', 'ProjectController@download');
    Route::match(['get', 'post'], '/project/add/task/{id}', 'ProjectController@addTask')->name('addTask');
    Route::match(['get', 'post'], '/project/add/developer/{id}', 'ProjectController@addDev')->name('addDev');
    Route::resource('project', 'ProjectController');
    Route::resource('task', 'TaskController');
});
