# Laravel PmForJira

# I) Подключение к БД

	0) Скачиваем проект и переименовуем корневую папку в "PmForJira".
	
	1) Размещаем на сервере.
	
	2) Пишем sudo chmod -R 777 /opt/lampp/htdocs/PmForJira.
	
	3) Переходми в папку "PmForJira" cd /opt/lampp/htdocs/PmForJira.
	
	4) Пишем composer install.
	
	5) Копируем из файла env.example в файл .env.
	
	6) прописуем php artisan key:generate
	
	7) В phpMyAdmin создаем базу "DB_PmForJira" выбираем кодироку 'utf8_general_ci'.
	
	8) прописуем в файле настройки базы .env DB_DATABASE=DB_PmForJira, DB_USERNAME=root, DB_PASSWORD=" "
	
	9) В терминал пишете  php artisan migrate.
	
	10) В терминал пишете  php artisan db:seed.
	
	11) Запускаем сервер.
	
	12) Перходим на старновй адресс http://localhost/PmForJira/public/.

# II)Подключение к Jira
	
	Свой сервер вписуем в файле .env 
	
	JIRA_URL = 'ссылка на сервер Jira'
	
	JIRA_USERNAME = 'логин от вашего сервера Jira'
	
	JIRA_PASSWORD = 'пароль от вашего сервера Jira'