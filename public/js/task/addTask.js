/**
 * Created by mentos1 on 15.08.17.
 */
$( function() {
    $( "#sortable1, #sortable2" ).sortable({
        connectWith: ".connectedSortable"
    }).disableSelection();

    $('img').click(function (e) {
        var data = {
            "_token": csrf_token,
            "project_id" : $("#project_Owner").attr("data-id")
        };
        $.ajax({
            type: "POST",
            url: BASEURL + "/project/add/task/" + $(this).attr('data-id'),
            data: data,
            dataType: 'json',
            success: function (response) {
                console.log(response.response);
                $("#div1").html(response.response);
                $('#dialog').dialog('open');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.dir(textStatus, errorThrown);
            }
        });
    });

    $( "#dialog" ).dialog({
        autoOpen: false,
        width: "45%",
        maxWidth: "400px",
        minWidth: "400px",
        modal: true,
        weekends: false,
        show:{
            effect: 'blind',
            duration: 500
        },
        hide:{
            effect: 'blind',
            duration: 500
        },
    });



    $('#addNewTask').click(function () {
        var data = {
            "_token": csrf_token,
            "project_id": $("#project_Owner").attr("data-id")
        };
        $.ajax({
            type: "POST",
            url: BASEURL + "/project/add/task/" + $(this).attr('data-id'),
            data: data,
            dataType: 'json',
            success: function (response) {
                console.log(response.response);
                $("#div1").html(response.response);
                $('#dialog').dialog('open');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.dir(textStatus, errorThrown);
            }
        });
    });

    $("#butSendToDb").click(function () {
        var d = $('#sortable2').find('img');
        var arr_idTask_for_send_to_controler = [];
        for(var i = 0; i < d.length; i++){
            arr_idTask_for_send_to_controler.push($($('#sortable2').find('img')[i]).attr('data-id'));
        }
        var data = {
            "_token": csrf_token,
            "tasks": arr_idTask_for_send_to_controler
        };

        var project_id = $("#project_Owner").attr("data-id");

        $.ajax({
            type: "GET",

            url: BASEURL + '/task/' + project_id + '/edit',
            data: data,
            dataType: 'json',
            success: function (response) {
                console.log(response.response);
                $('.alert-success').attr("style","display: block")

                function func() {
                    $('.alert-success').hide("slow" );
                }

                setTimeout(func, 2000);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.dir(this.url);
                console.dir(textStatus, errorThrown);
                $('.alert-danger').attr("style","display: block");

                function func() {
                    $('.alert-danger').hide("slow" );
                }

                setTimeout(func, 2000);
            }
        });
    });

} );