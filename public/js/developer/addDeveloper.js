/**
 * Created by mentos1 on 15.08.17.
 */
$( function() {
    $( "#sortable1, #sortable2" ).sortable({
        connectWith: ".connectedSortable"
    }).disableSelection();

    $('img').click(function (e) {
        var data = {
            "_token": csrf_token
        };

        $.ajax({
            type: "POST",
            url: BASEURL + "/project/add/developer/" + $(this).attr('data-id'),
            data: data,
            dataType: 'json',
            success: function (response) {
                console.log(response.response);
                $("#div1").html(response.response);
                $('#dialog').dialog('open');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.dir(textStatus, errorThrown);
            }
        });
    });

    $( "#dialog" ).dialog({
        autoOpen: false,
        width: "45%",
        maxWidth: "400px",
        minWidth: "400px",
        modal: true,
        weekends: false,
        show:{
            effect: 'blind',
            duration: 500
        },
        hide:{
            effect: 'blind',
            duration: 500
        },
    });

    $("#butSendToDb").click(function () {
        var d = $('#sortable2').find('img');
        var arr_idDev_for_send_to_controler = [];
        for(var i = 0; i < d.length; i++){
            arr_idDev_for_send_to_controler.push($($('#sortable2').find('img')[i]).attr('data-id'));
        }
        var data = {
            "_token": csrf_token,
            "developers": arr_idDev_for_send_to_controler
        };

        var project_id = $("#project_Owner").eq(0).attr("data-id");
        $.ajax({
            type: "get",
            url: BASEURL + "/developercheck/" + project_id,
            data: data,
            dataType: 'json',
            success: function (response) {
                if (response.response.length == 0) {
                    console.log(response.response);
                    $.ajax({
                        type: "GET",
                        url: BASEURL + "/developer/" + project_id + "/edit",
                        data: data,
                        dataType: 'json',
                        success: function (response) {
                            console.log(response.response);
                            $('.alert-success').show( "slow" );

                            function func() {
                                $('.alert-success').hide("slow" );
                            }
                            setTimeout(func, 2000);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.dir(textStatus, errorThrown);
                            $('.alert-danger').show( "slow" );

                            function func() {
                                $('.alert-danger').hide("slow" );
                            }

                            setTimeout(func, 2000);
                        }
                    });
                } else {
                    var answer = "";
                    for(var i =0;i < response.response.length; i++){
                        answer += "Developer " + response.response[i] + " is busy. \n";
                    }
                    alert(answer);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });

    });

} );