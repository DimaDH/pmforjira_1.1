$("#delete").on('click',function (e) {
    alert("delete");
    var data = {
        "_token": csrf_token
    };

    var project_id = $(".add_img").eq(0).attr("data-id");
    $.ajax({
        type: "DELETE",
        url: BASEURL + "/project/" + project_id,
        data: data,
        dataType: 'json',
        success: function (response) {
            if (response.response) {
                window.location.replace(BASEURL);
            } else {
                alert("Ошибка Удаления.");
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
});

$('#updateInfo').click(function (e) {
    alert("updateInfo")
    var data = {
        "_token": csrf_token
    };
    var project_id = $(".add_img").eq(0).attr("data-id");
    $.ajax({
        type: "post",
        url: BASEURL + "/updateProject/" + project_id,
        data: data,
        dataType: 'json',
        success: function (response) {
            $('#dialog2').empty();
            $("#dialog2").html(response.response);
            $("#dialog1").dialog('close');
            $("#dialog2").dialog('open');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.dir(textStatus, errorThrown);
        }
    });
});


$(".add_img").click(function () {
    if($(this).attr('data-goal') == 'dev'){
        var f = document.createElement("form");
        f.setAttribute('method',"get");
        f.setAttribute('id',"get");
        f.setAttribute('action', BASEURL + "/project/add/developer/" + $(this).attr('data-id'));


        var s = document.createElement("input"); //input element, Submit button
        s.setAttribute('type',"submit");
        s.setAttribute('value',"Submit");

        f.appendChild(s);
        document.getElementsByTagName('body')[0].appendChild(f);
        f.submit();
    }else{
        var f = document.createElement("form");
        f.setAttribute('method',"get");
        f.setAttribute('id',"get");
        f.setAttribute('action', BASEURL + "/project/add/task/" + $(this).attr('data-id'));

        var s = document.createElement("input"); //input element, Submit button
        s.setAttribute('type',"submit");
        s.setAttribute('value',"Submit");

        f.appendChild(s);
        document.getElementsByTagName('body')[0].appendChild(f);
        f.submit();


    }
});