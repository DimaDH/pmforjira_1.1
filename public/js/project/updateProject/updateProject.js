/**
 * Created by mentos1 on 15.08.17.
 */
function noWeekendsOrHolidays(date) {
    var noWeekend = $.datepicker.noWeekends(date);
    if (noWeekend[0]) {
        return nationalDays(date);
    } else {
        return noWeekend;
    }
}
$(this).ready(function () {
    $('#end-date').datepicker({ dateFormat: 'yy-mm-dd', beforeShowDay: noWeekendsOrHolidays });
    $('#start-date').datepicker({ dateFormat: 'yy-mm-dd', beforeShowDay: noWeekendsOrHolidays});
});
$('.ui-timepicker-input').timepicker({
    minTime: "10:00",
    maxTime: "19:00"
} );

natDays = [
    [1, 26, 'au'], [2, 6, 'nz'], [3, 17, 'ie'],
    [4, 27, 'za'], [5, 25, 'ar'], [6, 6, 'se'],
    [7, 4, 'us'], [8, 17, 'id'], [9, 7, 'br'],
    [10, 1, 'cn'], [11, 22, 'lb'], [12, 12, 'ke']
];

function nationalDays(date) {
    for (i = 0; i < natDays.length; i++) {
        if (date.getMonth() == natDays[i][0] - 1
            && date.getDate() == natDays[i][1]) {
            return [false, natDays[i][2] + '_day'];
        }
    }
    return [true, ''];
}


