/**
 * Created by mentos1 on 15.08.17.
 */
function reloadTaskFormDb() {
    $.ajax({
        url: BASEURL + "/reloadTaskFormDb",
        dataType:'json',
        type:'GET',
        success:function(html){
            $('#dialog').empty();
            $('#dialog').html(html.response);
            $( "#dialog" ).dialog({
                autoOpen: false,
                width: "45%",
                maxWidth: "40%",
                minWidth: "50%",
                modal: true,
                weekends: false,
                show:{
                    effect: 'blind',
                    duration: 500
                },
                hide:{
                    effect: 'blind',
                    duration: 500
                },
            });
            function noWeekendsOrHolidays(date) {
                var noWeekend = $.datepicker.noWeekends(date);
                if (noWeekend[0]) {
                    return nationalDays(date);
                } else {
                    return noWeekend;
                }
            }
            $('#end-date').datepicker({ dateFormat: 'yy-mm-dd', beforeShowDay: noWeekendsOrHolidays });
            $('#start-date').datepicker({ dateFormat: 'yy-mm-dd', beforeShowDay: noWeekendsOrHolidays});


            natDays = [
                [1, 26, 'au'], [2, 6, 'nz'], [3, 17, 'ie'],
                [4, 27, 'za'], [5, 25, 'ar'], [6, 6, 'se'],
                [7, 4, 'us'], [8, 17, 'id'], [9, 7, 'br'],
                [10, 1, 'cn'], [11, 22, 'lb'], [12, 12, 'ke']
            ];

            function nationalDays(date) {
                for (i = 0; i < natDays.length; i++) {
                    if (date.getMonth() == natDays[i][0] - 1
                        && date.getDate() == natDays[i][1]) {
                        return [false, natDays[i][2] + '_day'];
                    }
                }
                return [true, ''];
            }
            $('.clockpicker').clockpicker({
                autoclose: true,
            });

            $('#open_select_from_jira').click(function() {
                if (!$(this).is(':checked')) {
                    $('#project').attr('disabled', true);
                    $('#project').attr("required", false);
                    $('#nameProject').prop('readonly', false);
                    $('#nameProject').attr("required", true);
                }else{
                    $('#project').attr('disabled', false);
                    $('#project').attr('required', true);
                    $('#nameProject').prop('readonly', true);
                    $('#nameProject').attr("required", false);
                }
            }).click();
        }
    });
}