/**
 * Created by mentos1 on 26.07.17.
 */
function getMetaContent() {
    var metas = document.getElementsByTagName('meta');
    for(var i=0; i<metas.length; i++) {
        console.log(metas[i]);
        if(metas[i].getAttribute("name") == "csrf-token")
            return metas[i].getAttribute("content");
    }

    return "";
}


$('#downloadFromJira').click(function () {
    var data = {
        "_token": getMetaContent()
    };
    var $button = $(this);
    $button.removeClass('added').attr('disabled', 'disabled').attr('src', '../public/img/802.gif');

    $.ajax({
        type: "POST",
        url: BASEURL + "/download",
        data: data,
        dataType: 'json',
        success: function (response) {
            if (response.response) {
                console.log(response.response);
                reloadTaskFormDb();
                if ($('.fc-timelineDay-button , .fc-day-header > a ,  .fc-day-number , .fc-timelineSevenDay-button').hasClass('fc-state-active')) {
                    $('#calendar').fullCalendar('removeEventSource', {events: BASEURL + "/projectlist"});
                    $('#calendar').fullCalendar('removeEventSource', {events: BASEURL + "/project"});
                    $('#calendar').fullCalendar('addEventSource', BASEURL + "/project");
                }
                if ($('.fc-month-button , .fc-agendaWeek-button').hasClass('fc-state-active')) {
                    $('#calendar').fullCalendar('removeEventSource', {events: BASEURL + "/project"});
                    $('#calendar').fullCalendar('removeEventSource', {events: BASEURL + "/projectlist"});
                    $('#calendar').fullCalendar('addEventSource', BASEURL + "/projectlist");
                }
                var json_dev = BASEURL + "/developerlist";

                $('#calendar').fullCalendar({
                    resources: json_dev
                });
                $button.removeAttr('disabled').attr('src', '../public/img/reload.png');
                $('.alert-success').attr("style","display: block")

                function func() {
                    $('.alert-success').hide("slow" );
                }

                setTimeout(func, 2000);
            } else {
                console.log(response.response);
                $button.removeAttr('disabled').attr('src', '../public/img/reload.png');
                $('.alert-danger').attr("style","display: block");

                function func() {
                    $('.alert-danger').hide("slow" );
                }
                setTimeout(func, 2000);
                alert("Error.");
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $button.removeAttr('disabled').attr('src', '../public/img/reload.png');
            $('.alert-danger').attr("style","display: block");

            function func() {
                $('.alert-danger').hide("slow" );
            }
            setTimeout(func, 2000);
            console.log(textStatus, errorThrown);
        }
    });
});

setInterval(function(){
    // sent ajax to Controller for upload from Jira
    $("#downloadFromJira").trigger('click');
}, 60000 * 10);