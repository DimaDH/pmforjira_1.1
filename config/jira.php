<?php
return [
    'url'      => env('JIRA_URL','false'),
    'username' => env('JIRA_USERNAME','false'),
    'password' => env('JIRA_PASSWORD','false'),
];