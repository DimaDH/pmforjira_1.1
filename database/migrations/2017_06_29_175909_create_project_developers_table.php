<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectDevelopersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_developer', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('project_id')->unsigned()->default(1);
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');

            $table->integer('developer_id')->unsigned()->default(1);
            $table->foreign('developer_id')->references('id')->on('developers')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_developer');
    }
}
