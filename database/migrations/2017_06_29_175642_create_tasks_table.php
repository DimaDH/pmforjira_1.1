<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('summary')->default('none');
            $table->string('key', 100)->default('none');
            $table->string('color', 7)->nullable(true)->default('#000');

            $table->integer('priority_id')->unsigned()->default(1);
            $table->foreign('priority_id')->references('id')->on('priority_task')->onDelete('cascade');


            $table->integer('project_id')->unsigned()->default(1);
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');

            /*$table->integer('description_id')->unsigned()->default(1);
            $table->foreign('description_id')->references('id')->on('description_task')->onDelete('cascade');*/
            $table->string('resolution')->nullable(true)->default('none');
            $table->string('status')->default('none');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
