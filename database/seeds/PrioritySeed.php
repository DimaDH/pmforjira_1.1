<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PrioritySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('priority_task')->insert([
        'name' => 'Highest'
        ]);
        DB::table('priority_task')->insert([
            'name' => 'High'
        ]);
        DB::table('priority_task')->insert([
            'name' => 'Medium'
        ]);
        DB::table('priority_task')->insert([
            'name' => 'Low'
        ]);
        DB::table('priority_task')->insert([
            'name' => 'Lowest'
        ]);
    }
}
