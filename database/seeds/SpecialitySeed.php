<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SpecialitySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('specialies')->insert([
            'name' => 'PHP'
        ]);
        DB::table('specialies')->insert([
            'name' => 'Js'
        ]);
        DB::table('specialies')->insert([
            'name' => 'Angular'
        ]);
        DB::table('specialies')->insert([
            'name' => 'React js'
        ]);
        DB::table('specialies')->insert([
            'name' => 'Google api'
        ]);
        DB::table('specialies')->insert([
            'name' => 'Yandex api'
        ]);
        DB::table('specialies')->insert([
            'name' => 'Laravel'
        ]);
        DB::table('specialies')->insert([
            'name' => 'PayPal'
        ]);

    }
}
