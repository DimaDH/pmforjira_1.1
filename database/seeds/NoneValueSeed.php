<?php

use Illuminate\Database\Seeder;

class NoneValueSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    /*    DB::table('description_projects')->insert([
            'name' => 'none'
        ]);
        DB::table('description_task')->insert([
            'name' => 'none'
        ]);*/
        DB::table('projects')->insert([
            'name' => 'none',
            'start' => '1900-01-01 00:00:00',
            'end' => '1900-01-01 00:00:00'
        ]);
    }
}
