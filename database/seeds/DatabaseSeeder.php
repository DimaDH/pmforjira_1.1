<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LevelSeed::class);
        $this->call(PrioritySeed::class);
        $this->call(SpecialitySeed::class);
        $this->call(NoneValueSeed::class);
    }
}
