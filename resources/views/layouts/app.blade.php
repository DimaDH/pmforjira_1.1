<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    @section('style')
    <link href="../public/css/app.css" rel="stylesheet">
    <link href="../public/css/fc.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../public/lib/jquery.qtip.custom/jquery.qtip.min.css">

    <!-- Scripts -->
    <link href='../public/lib/fullcalendar.css' rel='stylesheet' />

    <!-- ClockPicker Stylesheet -->
    <link rel="stylesheet" type="text/css" href="../public/lib/clockpicker-gh-pages/dist/jquery-clockpicker.min.css">
    <link href='../public/lib/bootstrap-datetimepicker.min.css' rel='stylesheet' />
    <link href='../public/lib/fullcalendar.print.css' rel='stylesheet' media='print' />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href='../public/lib/fullcalendar-scheduler-1.6.2/scheduler.css' rel='stylesheet' />
    @show

    @section('script')
    <script src='../public/lib/jquery-ui-1.12.1.custom/external/jquery/jquery.js'></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src='../public/lib/lib/jquery.min.js'></script>
    <script src='../public/lib/lib/moment.min.js'></script>
    <script src='../public/lib/fullcalendar.js'></script>
    <script src='../public/lib/bootstrap-datetimepicker.min.js'></script>
    <script src='../public/lib/jquery.qtip.custom/jquery.qtip.min.js'></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src='../public/lib/fullcalendar-scheduler-1.6.2/scheduler.js'></script>
    <!-- ClockPicker script -->
    <script type="text/javascript" src="../public/lib/clockpicker-gh-pages/dist/bootstrap-clockpicker.min.js"></script>
    <script>
        $ = jQuery.noConflict(false);
    </script>
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script>
        var csrf_token = "{{ csrf_token() }}";
        var BASEURL = "{{ url('/') }}";
        $(window).on('load', function () {
            var $preloader = $('#page-preloader'),
                $spinner   = $preloader.find('.spinner');
                $spinner.fadeOut();
                $preloader.delay(350).fadeOut('slow');
        });
    </script>
    @show

    @section('myStyle')
    <style>
        #page-preloader {
            position: fixed;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            background: #f9f9f9;
            z-index: 100500;
        }

        #page-preloader .spinner {
            width: 64px;
            height: 64px;
            position: absolute;
            left: 50%;
            top: 45%;
            margin: -16px 0 0 -16px;
        }
        .fc-cell-content{
            height: 40px;
        }
        @media print {
            .fc-left, .fc-right,button,
            .panel-heading,.fc-now-indicator
            {
                display: none;
            }
        }
    </style>
    @show
</head>
    <body>
    @section('preloader')
        <div id="page-preloader"><span class="spinner"></span></div>
    @show
    @section('navBar')
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">&nbsp;
                        <li><a href="{{ url('/') }}">Distribution</a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    @show

    @section('content')
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">Distribution
                            <img id="downloadFromJira" src="../public/img/reload.png">
                            <img src="../public/img/print.png" id="print" onclick="printPdf()">
                        </div>
                        <div class="panel-body">
                            <div id='calendar'></div>
                            <div id='dialog'>
                            </div>
                            <div id='dialog1'>
                                <div class="modal-dialog">
                                    <div class="modal-header">
                                        <h3>Info Project</h3>
                                    </div>
                                    <div class="modal-body" id="div1">

                                    </div>
                                </div>
                            </div>
                            <div id='dialog2'>

                            </div>
                        </div>
                    </div>
                    <div class="alert alert-success col-md-6 col-md-offset-3">
                        Successfully added to the database.
                    </div>
                    <div class="alert alert-danger col-md-6 col-md-offset-3">
                        Error added to the database.
                    </div>
                </div>
            </div>
        @show

    <!-- Scripts -->
    @section('myScript')
        {{ Html::script('../public/js/project/reloadTaskFromDb.js') }}
        {{ Html::script('../public/js/project/settingCalendar.js') }}
        {{ Html::script('../public/js/project/settingDialog.js') }}
        {{ Html::script('../public/js/project/uploadJira.js') }}
        {{ Html::script('../public/js/project/settingPDF.js') }}
        {{ Html::script('../public/js/project/distribution.js') }}

    @show
</body>
</html>
