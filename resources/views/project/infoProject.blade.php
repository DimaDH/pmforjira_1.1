<div class="form-group row">
    <div class="container-fluid">
        <div class="col-md-6">
            @if(isset($project->name))
                <b>Title:</b><span id="title">{{ $project->name}}</span></br>
            @endif
            @if(isset($project->lead))
                <b>Lead:</b><span id="lead">{{ $project->lead}}</span></br>
            @endif
            @if(isset($project->start))
                <b>Start:</b><span id="start">{{ $project->start}}</span>
            @endif
            @if(isset($project->end))
                <b>End:</b><span id="end">{{ $project->end}}</span></br>
            @endif
        </div>
        <div class="col-md-3">
            <p>Developers <img class="add_img" data-goal="dev" data-id="{{ $project->id}}"   src="../public/img/plus-2.png"></p>
            <ul style="padding-left: 0">
                @if(isset($project->developers))
                    @foreach($project->developers as $pr)
                        <li>{{ $pr->title }}</li>
                    @endforeach
                @endif
            </ul>
        </div>
        <div class="col-md-3">
            <p>Tasks <img class="add_img" data-goal="task" data-id="{{ $project->id}}" src="../public/img/plus-2.png"></p>
            <ul style="padding-left: 0">
                @if(isset($project->tasks))
                    @foreach($project->tasks as $pr)
                        <li>{{ $pr->summary }}</li>
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
</div>
</div>
<div class="modal-footer">
        <button class ='btn btn-danger' id="delete">Delete</button>
        <button class ='btn btn-info'  id="updateInfo">Update Information</button>
</div>
{{ Html::script('../public/js/project/infoProject/infoProject.js') }}