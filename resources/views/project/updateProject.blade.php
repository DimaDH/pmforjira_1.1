<script type="text/javascript" src="../public/lib/timepicker/jquery-ui-timepicker-addon.js"></script>
<link rel="stylesheet" type="text/css" href="../public/lib/timepicker/jquery-ui-timepicker-addon.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">

{!! Form::open(['url' => '/updateWithSeparateDate', 'method' => 'post']) !!}
<div class="modal-dialog">
    <input type="hidden" name="id" value="{{ $project->id }}">
    <div class="modal-header">
        <h3>Add Project</h3>
    </div>
    <div class="modal-body">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Title<span style="color:red">*</span></label>
            <div class="col-sm-4">
                @if(isset($project))
                    <input type="text" id="nameProject" class="form-control" required value="{{ $project->name }}" name="title" placeholder="Name">
                @else
                    <input type="text" id="nameProject" class="form-control" required  name="title" placeholder="Name">
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Start<span style="color:red">*</span></label>
            <div class="col-sm-5">
                @if(isset($project))
                    <input type="date" class="form-control datepicker" id="start-date" name="start-date" value="{{ $project->startD }}" id="inputPassword" placeholder="Date">
                @else
                    <input type="date" class="form-control datepicker" id="start-date" name="start-date" id="inputPassword" placeholder="Date">
                @endif
                </div>
            <div class="col-sm-5">

                    <input type="text" class="form-control ui-timepicker-input" required value="{{ $project->startT }}"  name="start-time">

            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-2 col-form-label">End<span style="color:red">*</span></label>
            <div class="col-sm-5">
                @if(isset($project))
                    <input type="text" class="form-control datepicker" required id="end-date" value="{{ $project->endD }}" name="end-date" id="inputPassword" placeholder="Date">
                @else
                    <input type="text" class="form-control datepicker" required id="end-date" value="{{ $project->endD }}" name="end-date" id="inputPassword" placeholder="Date">
                @endif
            </div>
            <div class="col-sm-5">

                    <input type="text" class="form-control ui-timepicker-input" value="{{ $project->endT }}" required  name="end-time">

            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-2 col-form-label">Distribution</label>
            <div class="col-sm-10">
                <textarea class="form-control" rows="5" name="distribution" id="comment">{{ $project->descriptions }}</textarea>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        {!! Form::submit('Update', ['class' => 'btn btn-success']) !!}
    </div>
</div>
{!! Form::close() !!}
{{ Html::script('../public/js/project/updateProject/updateProject.js') }}