<style>
    .widthMax{
        width: 100%;
    }
    .widthHalf{
        width: 50%;
    }
</style>
<div class="modal-header">
    <h3>{{ $button }} Task</h3>
</div>
<form method="get" action="{{ url('/task/create') }}" id="form1">
@if(isset($task->id))
    <input type="hidden" name="id_Task"  value="{{ $task->id }}">
@endif
    <input type="hidden" name="project_id" value="{{ $project_id }}">
    <div class="modal-body">
    @if(isset($task->summary))
        <p><b>Summary<span style="color:red">*</span></b><input name="summary"  class="form-control form-control-sm widthMax" value="{{ $task->summary }}" required></p>
    @else
        <p><b>Summary<span style="color:red">*</span></b><input name="summary"  class="form-control form-control-sm widthMax" value="" required></p>
    @endif

    @if(isset($task->key))
        <p><b>Key</b><input class="widthMax"  name="key" value="{{ $task->key }}"></p>
    @else
        <p><b>Key</b><input class="widthMax"  name="key" value=""></p>
    @endif

{{--    <p><b>Color: </b><input type="color" class="widthMax" value="{{ $task->color }}"></p>--}}

    <p><b>Priority<span style="color:red">*</span></b>
            <select class="widthMax selectpicker show-tick" name="priority"  required>
            @if(isset($task->priority->name))
                @foreach(\App\PriorityTask::all() as $it)
                    @if($task->priority->name != $it->name)
                        <option value="{{ $it->id }}"><img src="../../../../public/img/{{ $it->name }}.png">{{ $it->name }}</option>
                    @else
                        <option selected value="{{ $it->id }}"><img src="../../../../public/img/{{ $it->name }}.png">{{ $it->name }}</option>
                    @endif
                @endforeach
            @else
                @foreach(\App\PriorityTask::all() as $it)
                        <option value="{{ $it->id }}"><img src="../../../../public/img/{{ $it->name }}.png">{{ $it->name }}</option>
                @endforeach
            @endif
            </select>
    </p>
    <p><b>Descriptions</b>
        @if(isset($task->descriptions[0]->name))
            <div class="widthMax layer">
                    @foreach($task->descriptions as $dTask)
                         <textarea name="descriptions" class="form-control" rows="5">{{ $dTask->name }}</textarea>
                    @endforeach
            </div>
        @else
            <textarea class="form-control" name="descriptions" rows="5"></textarea>
        @endif
    </p>
    @if(isset($task->resolution))
        <p><b>Resolution</b><textarea name="resolution" class="form-control" rows="5">{{ $task->resolution }}</textarea></p>
    @else
            <p><b>Resolution</b><textarea name="resolution" class="form-control" rows="5"></textarea></p>
    @endif
    <p>
    @if(isset($task->resolution))
        <b>Status</b>
            {{--<span style="background-color: #232985; margin-left: 10px; color: white;position: absolute;border-radius: 2px; padding: 0 2px 0 2px;">{{ $task->status }}</span>--}}
            <input class="widthHalf" name="status" type="text" value="{{ $task->status }}">

    @else
        <b>Status<span style="color:red">*</span></b>
        <input class="widthHalf" name="status" type="text" required>
    @endif
    </p>
    <p>
    @if(isset($task->specialities[0]->name))
        <b>Label</b>
            <input name="jurors" id="jurors" type="text" style="width: 50%;">
            <input type="button" value="->" class="addButton" style="width: 10%;" onclick="add_D();" /> <!-- The 'onclick="add();" will call a the Javascript function add() -->
            <select id="mySelect" name="labels[]" style="width:30%; vertical-align: top; border: 1px solid #b4b4b4; border-radius: 10px" rows="1" multiple="multiple">
            @foreach($task->specialities as $it)
                <option value="{{ $it->name }}">{{ $it->name }}</option>
            @endforeach
            </select>
        </b>
    @else
        <b style="width: 10%">Label:</b>
        <input name="jurors" id="jurors" type="text" style="width: 50%;">
        <input type="button" value="->" class="addButton" style="width: 10%;" onclick="add_D();" /> <!-- The 'onclick="add();" will call a the Javascript function add() -->
        <select id="mySelect"  name="labels[]" style="width:30%; vertical-align: top; border: 1px solid #b4b4b4; border-radius: 10px" rows="1" multiple="multiple"></select>
    @endif
    </p>
</div>
<div class="modal-footer">
    <button class ='btn btn-default' id="close">Close</button>
    @if($button == 'Update')
        <button class ='btn btn-danger' data-id ="{{$task->id}}" id="delete">Delete</button>
    @endif
    <button class ='btn btn-info'  id="add">{{ $button }}</button>
</div>
</form>
{{ Html::script('../public/js/task/task.js') }}