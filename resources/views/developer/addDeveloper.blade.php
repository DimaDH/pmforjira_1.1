@extends('layouts.app')

@section('style')
    <link href="../../../../public/css/app.css" rel="stylesheet">
    <link href="../../../../public/css/fc.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../../../../public/lib/jquery.qtip.custom/jquery.qtip.min.css">

    <!-- Scripts -->
    <link href='../../../../public/lib/fullcalendar.css' rel='stylesheet' />

    <!-- ClockPicker Stylesheet -->
    <link href='../../../../public/lib/bootstrap-datetimepicker.min.css' rel='stylesheet' />
    <link href='../../../../public/lib/fullcalendar.print.css' rel='stylesheet' media='print' />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href='../../../../public/lib/fullcalendar-scheduler-1.6.2/scheduler.css' rel='stylesheet' />
@endsection

@section('script')
    <script src='../../../../public/lib/jquery-ui-1.12.1.custom/external/jquery/jquery.js'></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src='../../../../public/lib/lib/jquery.min.js'></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- ClockPicker script -->
    <script>
        $ = jQuery.noConflict(false);
    </script>
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script>
        var csrf_token = "{{ csrf_token() }}";
        var BASEURL = "{{ url('/') }}"; // BASEURL + "/distribution" and + "/developerlist" //заменить на роут
        $(window).on('load', function () {
            var $preloader = $('#page-preloader'),
                    $spinner   = $preloader.find('.spinner');
            $spinner.fadeOut();
            $preloader.delay(350).fadeOut('slow');
        });
    </script>
@endsection


@section('myStyle')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        .layer {
            overflow: scroll;
            width : 300px;
            height : 150px;
            padding : 2px;
            overflow-x: hidden;
        }
    </style>
    <style>
        #sortable1, #sortable2 {
            border: 1px solid #eee;
            width: 142px;
            min-height: 220px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }
        #sortable1 li, #sortable2 li {
            margin: 0 5px 5px 5px;
            padding: 5px;
            font-size: 1.2em;
            width: 120px;
        }
        .alert{
            display: none;
        }
    </style>
@endsection

@section('preloader')
    @parent
@endsection

@section('navBar')
    @parent
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Developer</div>

                    <div class="panel-body row">
                        <div class="col-md-3">
                            <ul id="sortable1" class="connectedSortable">
                                @if(isset($developers))
                                    @foreach($developers as $developer)
                                        @if(!$project->developers->contains($developer))
                                            <li class="ui-state-default" style="position: relative"><p>{{ $developer->title }} <img style="position: absolute; bottom: 2px; right: 2px; float: right; width: 15%;" src="../../../../public/img/curriculum.png" data-id="{{ $developer->id }}"></p></li>
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <ul id="sortable2" class="connectedSortable">
                                @if(isset($project))
                                    @foreach($project->developers as $developer)
                                        <li class="ui-state-default" style="position: relative"><p>{{ $developer->title }} <img style="position: absolute; bottom: 2px; right: 2px; float: right; width: 15%;" src="../../../../public/img/curriculum.png" data-id="{{ $developer->id }}"></p></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <p><b>Owner  </b><img src="../../../../public/img/user.png" id="project_Owner" data-id = "{{ $project->id }}">{{ $project->lead }}</p>
                            <p><b>Title:</b>{{ $project->name }}</p>
                            <p><b>Descriptions:</b>
                                <div class="layer">
                                    <ul>
                                        @foreach($project->descriptions as $description)
                                            @if($dProject->name !== "")
                                                <li><p>{{ $description->name }}</p></li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </p>
                            <p><b>Start:</b>{{ $project->start }}</p>
                            <p><b>End:</b>{{ $project->end }}</p>
                            <p style="float: right"><button id="butSendToDb" class="btn btn-success">Add</button></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="alert alert-success col-md-6 col-md-offset-3">
        Successfully added to the database.
    </div>
    <div class="alert alert-danger col-md-6 col-md-offset-3">
        Error added to the database.
    </div>
    <div id='dialog'>
        <div class="modal-dialog">
            <div class="modal-header">
                <h3>Info Developer</h3>
            </div>
            <div class="modal-body" id="div1">
            </div>
            <div class="modal-footer">
                <button class ='btn btn-danger' id="delete">Delete</button>
                <button class ='btn btn-info' id="moreInfo">More Information</button>
            </div>
        </div>
    </div>
@endsection

@section('myScript')
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    {{ Html::script('../public/js/developer/addDeveloper.js') }}
@endsection

